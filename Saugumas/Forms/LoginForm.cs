﻿using Saugumas.Models;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace Saugumas
{
    public partial class LoginForm : Form
    {
        public static UserModel UserModel;
        public static LoginForm Instance;
        private readonly string fileText = "Labas mano vardas Arnas";
        public LoginForm()
        {
            Instance = this;
            InitializeComponent();
            FileCreation();
        }

        private void ButtonRegister_Click(object sender, EventArgs e)
        {
            RegisterForm registerForm = new RegisterForm();

            registerForm.Show();
        }

        private void FileCreation()
        {
            if (!File.Exists("File.txt"))
            {
                File.Create("File.txt").Close();
                StreamWriter sw = File.AppendText("File.txt");
                sw.WriteLine(fileText);
                sw.Close();
            }

            StreamReader check = new StreamReader("File.txt");
            var text = check.ReadLine();
            check.Close();

            if (!text.Contains(fileText))
            {
                StreamReader sr = new StreamReader("File.txt");

                var username = sr.ReadLine();
                sr.Close();

                string decryptUser = AesCrypt.Decrypt(username);

                File.WriteAllText("File.txt", decryptUser);
            }
        }

        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Login();
                UserForm userForm = new UserForm();
                userForm.Show();
                DecryptUserFile();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void DecryptUserFile()
        {
            StreamReader sr = new StreamReader(txtUsername.Text + ".txt");

            var username = sr.ReadLine();
            var password = sr.ReadLine();
            sr.Close();

            if (password != txtPassword.Text)
            {
                File.WriteAllText(txtUsername.Text + ".txt", string.Empty);

                string decryptpassword = AesCrypt.Decrypt(password);

                StreamWriter sw = File.AppendText(txtUsername.Text + ".txt");
                sw.WriteLine(username);
                sw.WriteLine(decryptpassword);
                sw.Close();
            }
        }

        private UserModel Login()
        {
            try
            {
                if (txtUsername.Text.Length <= 0 && txtPassword.Text.Length <= 0)
                    throw new Exception("Password and Username cant be empty fields");

                SqlConnection connection = new SqlConnection("Data Source=LAPTOP-AV3DN7CM\\SQLEXPRESS;Initial Catalog=user_db;Integrated Security=True");
                var selectUser = "select id, username, password, description from users where username = @username and password=@password";

                var command = new SqlCommand(selectUser, connection);

                command.Parameters.AddWithValue("@username", txtUsername.Text);
                command.Parameters.AddWithValue("@password", MD5.Encrypt(txtPassword.Text));

                connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = int.Parse(reader["id"].ToString());
                        var username = reader["username"].ToString();
                        var password = reader["password"].ToString();
                        var decryptPass = MD5.Decrypt(password);
                        var description = reader["description"].ToString();
                        connection.Close();

                        UserModel = new UserModel(id, username, decryptPass, description);

                        return UserModel;
                    }
                }

                connection.Close();

                throw new Exception("Wrong Username Or Password");
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message);
            }
        }
    }
}
