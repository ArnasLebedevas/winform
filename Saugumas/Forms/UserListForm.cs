﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace Saugumas.Forms
{
    public partial class UserListForm : Form
    {
        private readonly SqlConnection connection = new SqlConnection("Data Source=LAPTOP-AV3DN7CM\\SQLEXPRESS;Initial Catalog=user_db;Integrated Security=True");

        public UserListForm()
        {
            InitializeComponent();
        }

        private void UserListForm_Load(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                DisplayingUsersInListView();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.ExitThread();
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                buttonDeleteUser.Enabled = true;
            }
            else
            {
                buttonDeleteUser.Enabled = false;
            }
        }

        private void DisplayingUsersInListView()
        {
            listView1.Items.Clear();

            SqlCommand command = new SqlCommand("SELECT * FROM users ORDER BY username", connection);

            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    ListViewItem items = new ListViewItem(dataReader["username"].ToString());
                    items.SubItems.Add(dataReader["password"].ToString());
                    items.SubItems.Add(dataReader["description"].ToString());
                    listView1.Items.Add(items);
                }
                dataReader.Close();
                dataReader.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ButtonDeleteUser_Click(object sender, EventArgs e)
        {
            string username = listView1.SelectedItems[0].Text;
            string file = listView1.SelectedItems[0].Text.Trim() + ".txt";

            SqlCommand commandUserDelete = new SqlCommand("DELETE FROM users WHERE username = @username", connection);
            commandUserDelete.Parameters.AddWithValue("@username", username);
            try
            {
                commandUserDelete.ExecuteNonQuery();
                DisplayingUsersInListView();
                buttonDeleteUser.Enabled = false;
                DeleteFile(file);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DeleteFile(string file)
        {
            if (File.Exists(file))
                File.Delete(file);
        }

        private void TxtName_TextChanged(object sender, EventArgs e)
        {
            if(txtName.Text.Length > 0)
            {
                FilterByUsername();
            }
            else
            {
                DisplayingUsersInListView();
            }
        }

        private void FilterByUsername()
        {
            listView1.Items.Clear();

            SqlCommand command = new SqlCommand("Select * from users WHERE username = @username", connection);
            command.Parameters.AddWithValue("@username", txtName.Text);
            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    ListViewItem items = new ListViewItem(dataReader["username"].ToString());
                    items.SubItems.Add(dataReader["password"].ToString());
                    items.SubItems.Add(dataReader["description"].ToString());
                    listView1.Items.Add(items);
                }
                dataReader.Close();
                dataReader.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ButtonShow_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();

            SqlCommand command = new SqlCommand("SELECT * FROM users ORDER BY username", connection);

            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    ListViewItem items = new ListViewItem(dataReader["username"].ToString());
                    var paasowrd = dataReader["password"].ToString();
                    var decryptPass = MD5.Decrypt(paasowrd);
                    items.SubItems.Add(decryptPass);
                    items.SubItems.Add(dataReader["description"].ToString());
                    listView1.Items.Add(items);
                }
                dataReader.Close();
                dataReader.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
