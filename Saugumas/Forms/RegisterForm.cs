﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace Saugumas
{
    public partial class RegisterForm : Form
    {
        private readonly SqlConnection connection = new SqlConnection("Data Source=LAPTOP-AV3DN7CM\\SQLEXPRESS;Initial Catalog=user_db;Integrated Security=True");

        public RegisterForm()
        {
            InitializeComponent();
        }

        private void ButtonRegister_Click(object sender, EventArgs e)
        {
            try
            {
                RegisterUser(txtUsername.Text, txtPassword.Text, txtDescription.Text);
                this.Close();
            }
            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        public void InsertUser(string username, string password, string description)
        {
            try
            {
                connection.Open();
                string insertQuery = "insert into users (username, password, description) values (@username, @password, @description)";
                SqlCommand com = new SqlCommand(insertQuery, connection);
                com.Parameters.AddWithValue("@username", username);
                com.Parameters.AddWithValue("@password", MD5.Encrypt(password));
                com.Parameters.AddWithValue("@description", description);
                com.ExecuteNonQuery();
                connection.Close();

                CreateFileForUser();
                MessageBox.Show("Registration complete: " + txtUsername.Text);
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message);
            }
        }

        public void RegisterUser(string username, string password, string description)
        {
            try
            {
                if (txtUsername.Text.Length <= 3 && txtPassword.Text.Length <= 3)
                    throw new Exception("Password and Username must contain 3 characters");

                using (var command = new SqlCommand("select * from users where username=@username", connection))
                {
                    command.Parameters.AddWithValue("@username", username);

                    using (var dataAdapter = new SqlDataAdapter(command))
                    {
                        var dataTable = new DataTable();
                        dataAdapter.Fill(dataTable);

                        if (dataTable.Rows.Count >= 1)
                        {
                            MessageBox.Show("User already exist");

                            return;
                        }

                        InsertUser(username, password, description);
                    }
                }
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message);
            }
        }

        private void CreateFileForUser()
        {
            if(!File.Exists(txtUsername.Text + ".txt"))
            {
                File.Create(txtUsername.Text + ".txt").Close();
                StreamWriter sw = File.AppendText(txtUsername.Text + ".txt");
                sw.WriteLine(txtUsername.Text);
                sw.WriteLine(AesCrypt.Encrypt(txtPassword.Text));
                sw.Close();
            }
        }
    }
}
