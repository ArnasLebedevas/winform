﻿using Saugumas.Forms;
using System;
using System.IO;
using System.Windows.Forms;

namespace Saugumas
{
    public partial class UserForm : Form
    {
        public UserForm()
        {
            InitializeComponent();

            loginUsername.Text = "Username: " + LoginForm.UserModel.Username;
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader("File.txt");

            var username = sr.ReadLine();
            sr.Close();

            string encryptUser = AesCrypt.Encrypt(username);

            File.WriteAllText("File.txt", encryptUser);
            this.Close();
        }

        private void ButtonUsers_Click(object sender, EventArgs e)
        {
            UserListForm userListForm = new UserListForm();
            userListForm.Show();
        }

        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            UserEditForm userEditForm = new UserEditForm();
            userEditForm.Show();
        }
    }
}
