﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Saugumas.Forms
{
    public partial class UserEditForm : Form
    {
		private readonly SqlConnection connection = new SqlConnection("Data Source=LAPTOP-AV3DN7CM\\SQLEXPRESS;Initial Catalog=user_db;Integrated Security=True");

		public UserEditForm()
        {
            InitializeComponent();
        }

        private void ButtonPasswordChange_Click(object sender, EventArgs e)
        {
            try
            {
				UserPasswordChange(txtPassword.Text);
			}
            catch (Exception exc)
            {
				MessageBox.Show(exc.Message);
            }
		}

        private void ButtonGeneratePassword_Click(object sender, EventArgs e)
        {
            txtPassword.Text = CreatePassword(10);
        }

		public void UserPasswordChange(string newPassword)
		{
			try
			{
				string userName = LoginForm.UserModel.Username;

				var selectUser = "select * from users where username = @username";

				SqlCommand selectUserCommand = new SqlCommand(selectUser, connection);

				selectUserCommand.Parameters.AddWithValue("@username", userName);

				connection.Open();
				using (SqlDataReader dataReader = selectUserCommand.ExecuteReader())
				{
					if (dataReader.Read())
					{
						if (string.IsNullOrEmpty(newPassword))
							throw new Exception("Field is empty");

						connection.Close();

						UpdateUserPassword(newPassword, userName);
						ChangeUserTextFile(newPassword);
					}
					else
					{
						throw new Exception("Invalid Password Match");
					}
				}
			}
			catch (Exception exc)
			{
				throw new Exception(exc.Message);
			}
		}

		private void ChangeUserTextFile(string newPassword)
		{
			string file = LoginForm.Instance.txtUsername.Text;

			File.WriteAllText(file + ".txt", string.Empty);

			StreamWriter sw = File.AppendText(file + ".txt");
			sw.WriteLine(file);
			sw.WriteLine(newPassword);
			sw.Close();
		}

		private void UpdateUserPassword(string newPassword, string userName)
		{
			try
			{
				string encryptPassword = MD5.Encrypt(newPassword);

				var updateUser = "update users set password = @password where username = @username";

				SqlCommand updateUserCommand = new SqlCommand(updateUser, connection);

				updateUserCommand.Parameters.AddWithValue("@password", encryptPassword);
				updateUserCommand.Parameters.AddWithValue("@username", userName);

				connection.Open();
				updateUserCommand.ExecuteNonQuery();
				connection.Close();

				MessageBox.Show("Password Changed Successfully: " + newPassword);
			}
			catch (Exception exc)
			{
				throw new Exception(exc.Message);
			}
		}

		public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

            StringBuilder res = new StringBuilder();

            Random rnd = new Random();

            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }

            return res.ToString();
        }

        private void ButtonCopy_Click(object sender, EventArgs e)
        {
			Clipboard.SetText(txtPassword.Text);
        }
    }
}
