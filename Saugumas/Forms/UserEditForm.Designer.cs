﻿
namespace Saugumas.Forms
{
    partial class UserEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.ButtonPasswordChange = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonGeneratePassword = new System.Windows.Forms.Button();
            this.ButtonCopy = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(109, 50);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(189, 20);
            this.txtPassword.TabIndex = 1;
            // 
            // ButtonPasswordChange
            // 
            this.ButtonPasswordChange.Location = new System.Drawing.Point(258, 89);
            this.ButtonPasswordChange.Name = "ButtonPasswordChange";
            this.ButtonPasswordChange.Size = new System.Drawing.Size(104, 23);
            this.ButtonPasswordChange.TabIndex = 2;
            this.ButtonPasswordChange.Text = "Change";
            this.ButtonPasswordChange.UseVisualStyleBackColor = true;
            this.ButtonPasswordChange.Click += new System.EventHandler(this.ButtonPasswordChange_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "New password:";
            // 
            // ButtonGeneratePassword
            // 
            this.ButtonGeneratePassword.Location = new System.Drawing.Point(148, 89);
            this.ButtonGeneratePassword.Name = "ButtonGeneratePassword";
            this.ButtonGeneratePassword.Size = new System.Drawing.Size(104, 23);
            this.ButtonGeneratePassword.TabIndex = 4;
            this.ButtonGeneratePassword.Text = "Random";
            this.ButtonGeneratePassword.UseVisualStyleBackColor = true;
            this.ButtonGeneratePassword.Click += new System.EventHandler(this.ButtonGeneratePassword_Click);
            // 
            // ButtonCopy
            // 
            this.ButtonCopy.Location = new System.Drawing.Point(38, 89);
            this.ButtonCopy.Name = "ButtonCopy";
            this.ButtonCopy.Size = new System.Drawing.Size(104, 23);
            this.ButtonCopy.TabIndex = 5;
            this.ButtonCopy.Text = "Copy";
            this.ButtonCopy.UseVisualStyleBackColor = true;
            this.ButtonCopy.Click += new System.EventHandler(this.ButtonCopy_Click);
            // 
            // UserEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 143);
            this.Controls.Add(this.ButtonCopy);
            this.Controls.Add(this.ButtonGeneratePassword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonPasswordChange);
            this.Controls.Add(this.txtPassword);
            this.Name = "UserEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UserEditForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button ButtonPasswordChange;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonGeneratePassword;
        private System.Windows.Forms.Button ButtonCopy;
    }
}