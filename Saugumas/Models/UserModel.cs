﻿namespace Saugumas.Models
{
    public class UserModel
    {
        public UserModel(int id, string username, string password, string description)
        {
            ID = id;
            Username = username;
            Password = password;
            Description = description;
        }

        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
    }
}
